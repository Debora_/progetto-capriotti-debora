from sklearn.model_selection import KFold

from sklearn.metrics import mean_squared_error
from sklearn.metrics import precision_score, recall_score

import numpy as np
import json

logs = []
precision_scores = []
recall_scores = []

with open("Ground_Truth.txt", "r") as f:
    y = json.loads(f.readlines()[0])
    y = np.array(y) 

with open("Scores.txt", "r") as f:
    X = json.loads(f.read())
    X = np.array(X)

with open("Image_Vocabs.txt", "r") as f:
	image_voc = f.readlines()
	image_voc = json.loads(image_voc[0])[0]

y_pred = []
for key in image_voc:
    for value in image_voc[key][0]:
        y_pred.append(value)

kf = KFold(n_splits=5, random_state=None, shuffle=False)
for train_index, test_index in kf.split(X):
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    precision_scores.append(precision_score(y_test, y_pred, average='micro'))
    recall_scores.append(recall_score(y_test, y_pred, average='micro'))
    logs.append(mean_squared_error(y_test, y_pred))    

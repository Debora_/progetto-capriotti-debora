from __future__ import print_function
import cv2 as cv
import numpy as np
import argparse
#from time import time
from math import sqrt
import random

##
import csv
import os
import json
from collections import defaultdict
##

###
from sklearn.metrics import precision_score, recall_score
###

HIGHER_INLIERS = 0

###
def performance(predictions):

    with open("ground_truth.txt", "r") as f:
        truth = json.loads(f.readlines()[0])
    #print(truth)

    with open("performance.txt", "w") as f:
        f.write(
            "micro precision: "
            + str(precision_score(truth, predictions, average="micro"))
            + "\nmacro precision: "
            + str(precision_score(truth, predictions, average="macro"))
            + "\nmicro recall: "
            + str(recall_score(truth, predictions, average="micro"))
            + "\nmacro recall: "
            + str(recall_score(truth, predictions, average="macro"))
        )

    return
###



def init(nn_matches, img1, img2):
    global HIGHER_INLIERS 

    ##
    global SRC_PTS
    global DST_PTS
    ##

    ## [ratio test filtering]
    matched1 = []
    matched2 = []
    good = []
    nn_match_ratio = 0.8 # Nearest neighbor matching ratio
    for m, n in nn_matches:
        if m.distance < nn_match_ratio * n.distance:
            matched1.append(kpts1[m.queryIdx])
            matched2.append(kpts2[m.trainIdx])
            good.append(m)
            #print(good)
    ## [ratio test filtering]

    # choosing 4 random points
    points = []
    for i in range(4):
        p = random.choice(good)
        points.append(p)


    ## extract the matched keypoints
    src_pts = np.float32([ kpts1[m.queryIdx].pt for m in points ]).reshape(-1,1,2)
    dst_pts = np.float32([ kpts2[m.trainIdx].pt for m in points ]).reshape(-1,1,2)

    ## find homography matrix and do perspective transform
    H = cv.getPerspectiveTransform(src_pts, dst_pts) 
    #print(H)

    ## [homography check]
    inliers1 = []
    inliers2 = []
    good_matches = []
    inlier_threshold = 2.5 # Distance threshold to identify inliers with homography check
    for i, m in enumerate(matched1):
        col = np.ones((3,1), dtype=np.float64)
        col[0:2,0] = m.pt

        col = np.dot(H, col)
        col /= col[2,0]
        dist = sqrt(pow(col[0,0] - matched2[i].pt[0], 2) +\
                    pow(col[1,0] - matched2[i].pt[1], 2))

        if dist < inlier_threshold:
            good_matches.append(cv.DMatch(len(inliers1), len(inliers2), 0))
            inliers1.append(matched1[i])
            inliers2.append(matched2[i])
            #print(good_matches)

    ## [draw final matches]
    res = np.empty((max(img1.shape[0], img2.shape[0]), img1.shape[1]+img2.shape[1], 3), dtype=np.uint8)
    cv.drawMatches(img1, inliers1, img2, inliers2, good_matches, res)

    ##
    ## extract the matched keypoints
    source_pts = np.float32([ kpts1[m.queryIdx].pt for m in good_matches ]).reshape(-1,1,2)
    destin_pts = np.float32([ kpts2[m.trainIdx].pt for m in good_matches ]).reshape(-1,1,2)
    ##

    #inlier_ratio = len(inliers1) / float(len(matched1))

    if len(inliers1) > HIGHER_INLIERS:
        HIGHER_INLIERS = len(inliers1)

        ##
        SRC_PTS = source_pts
        DST_PTS = destin_pts
        ##
        
    return HIGHER_INLIERS, SRC_PTS, DST_PTS



if __name__ == "__main__":
    ## [load]
    parser = argparse.ArgumentParser(description='Code for SIFT local features matching tutorial.')
    parser.add_argument('--feature', help='Feature extractor type.', default='SIFT')

    ##
    parser.add_argument('--input', help='input to RANSAC.', default='input_RANSAC.txt')
    ##

    args = parser.parse_args()


    ##

    with open(os.path.join("input_RANSAC.txt")) as f:
	    images_i = f.readlines()
	    images_i = json.loads(images_i[0])
    #print(images_i)

    images = []
    for key in images_i:
        key = key.split("\\")[-1]
        images.append(key)
    #print(images)

    scores = defaultdict() 
   
   

    for i in range(len(images)): 
        img1 = cv.imread('./data/images/' + images[i], cv.IMREAD_GRAYSCALE)
        dict_img2 = images_i[images[i]]
        #print(dict_img2)

        for key in dict_img2:
            #print(key)
            img2 = cv.imread('./data/images/'+ key, cv.IMREAD_GRAYSCALE)
            #print(img1, img2) 
            if img1 is None or img2 is None:
                print('Could not open or find the images!')                
                exit(0)

    ##


            #start = time()
            extractor = cv.SIFT_create() 
            kpts1, desc1 = extractor.detectAndCompute(img1, None)
            kpts2, desc2 = extractor.detectAndCompute(img2, None)
            #end = time()

            matcher = cv.DescriptorMatcher_create(cv.DescriptorMatcher_BRUTEFORCE_L1)
            nn_matches = matcher.knnMatch(desc1, desc2, 2)
            #print("Extraction time: {end-start}")
                   
            for k in range(100):
                inliers, src_pts, dst_pts = init(nn_matches, img1, img2)

            ## find homography matrix considering all inliers
            #M, _ = cv.findHomography(src_pts, dst_pts, 0)
            #print(M)


            ##
            key = key.split("\\")[-1]
            if(int(key.split(".")[0]) - int(images[i].split(".")[0]) >= 80): 
                scores[images[i], key] = dict_img2[key] #score 
            ##


    ##
    #print(scores) 
    with open("output.csv", "w") as f:
        writer = csv.writer(f)
        for key in scores.keys():
            if(scores[key] >= 0.75):
                f.write(str(key) + ':' + str(scores[key]) + '\n')
    ##
    


    ###
    with open("images_loop.txt", "r") as f:
        images_loop = json.loads(f.readlines()[0])
    #print(images_loop)

    predictions_list = []
    images_2 = []
    for image1 in images_loop:
        #print(image1) 
        images_l1 = images_loop[image1] 
        #print(images_l1) 
        for image2 in images_i:
            #print(image2) 
            images_l2 = images_i[image2]
            for key in images_l2:
                images_2.append(key)
            #print(images_l)
            if(int(image1.split(".")[0]) == int(image2.split(".")[0])): 
                #print(images_l1)
                #print(images_l)
                for image_l1 in images_l1:
                    #print(image_l1)
                    if image_l1 in images_2:
                        predictions_list.append("1")
                    else:
                        predictions_list.append("0")
    
    #print(predictions_list)

    performance(predictions_list)
    ###
    
